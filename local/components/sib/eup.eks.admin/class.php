<?php

use Bitrix\Main\Application;
use Bitrix\Main\Entity\ReferenceField;
use Bitrix\Main\FileTable;
use Bitrix\Main\SystemException;
use Bitrix\Main\Loader;
use Bitrix\Main\ORM\Entity as ORMEntity;
use Bitrix\Main\ORM\Query\Result as QueryResult;
use Bitrix\Main\UI\PageNavigation;
use Bitrix\Main\Grid;
use Bitrix\Main\Type\Date;
use Bitrix\Main\Type\DateTime;
use Sibintek\Exchange\EKSEUP\Exception as EKSEUPException;
use Sibintek\Exchange\EKSEUP\ImportTable;
use Sibintek\Exchange\EKSEUP\ImportLog;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

class EKSEUPAdminComponent extends CBitrixComponent
{

    private
        $gridOptions,
        $navParams,
        $filter,
        $sort;

    public function executeComponent()
    {
        try {
            if (Loader::includeModule('sib.exchange.ekseup') !== true) {
                throw new SystemException('Ошибка подключения модуля sib.exchange.ekseup');
            }

            $this->arResult['GRID_ID'] = 'grid_' . $this->arParams['TYPE'] . '_files';

            $this->gridOptions = new Grid\Options($this->arResult['GRID_ID']);

            $this->arResult['SHOW_ROW_ACTIONS_MENU'] = true;

            $this->setHeaders();
            $this->setNavParams();
            $this->setFilter();
            $this->setSort();
            $this->setElements();
        } catch (\Exception $e) {
            throw new SystemException('Ошибка');
        }

        $this->includeComponentTemplate();
    }

    public function onPrepareComponentParams($params)
    {
        $contentType = strtolower($params['TYPE']);

        if (($contentType !== 'eks') && ($contentType !== 'eup')) {
            throw new SystemException('Не указан источник данных (ЕКС или ЕУП)');
        }

        $params['TYPE'] = $contentType;

        return $params;
    }

    private function setHeaders(): void
    {
        $this->arResult['HEADERS'] = [
            [
                'id' => 'ID',
                'name' => 'ID',
                'sort' => false,
                'default' => true,
                'editable' => false,
                'actions' => [],
            ],
            [
                'id' => 'FILE_DATE',
                'name' => 'Дата создания файла',
                'sort' => false,
                'default' => true,
                'editable' => false,
                'actions' => [],
            ],
            [
                'id' => 'CONTENT_TYPE',
                'name' => 'Тип',
                'sort' => false,
                'default' => true,
                'editable' => false,
                'actions' => [],
            ],
            [
                'id' => 'UPLOAD_DATE',
                'name' => 'Дата сохранения файла в АИСКУ',
                'sort' => false,
                'default' => true,
                'editable' => false,
                'actions' => [],
            ],
            [
                'id' => 'LAST_MODIFIED',
                'name' => 'LAST_MODIFIED',
                'sort' => false,
                'default' => true,
                'editable' => false,
                'actions' => [],
            ],
            [
                'id' => 'IMPORT_DATE',
                'name' => 'Дата загрузки данных в БД',
                'sort' => false,
                'default' => true,
                'editable' => false,
                'actions' => [],
            ],
            [
                'id' => 'IMPORT_RESULT',
                'name' => 'Результат загрузки',
                'sort' => false,
                'default' => true,
                'editable' => false,
                'actions' => [],
            ],
            [
                'id' => 'IMPORT_LOG_FILE',
                'name' => 'Лог импорта',
                'sort' => false,
                'default' => true,
                'editable' => false,
                'actions' => [],
            ],
        ];
    }

    private function setNavParams(): void
    {
        $this->navParams = $this->gridOptions->GetNavParams();

        $nav = new PageNavigation($this->arResult['GRID_ID']);
        $nav->allowAllRecords(false)->setPageSize($this->navParams['nPageSize'])->initFromUri();
        $nav->setPageSize($this->navParams['nPageSize']);

        $this->navParams['offset'] = $nav->getOffset();

        $this->arResult['NAV_OBJECT'] = $nav;

        $this->arResult['GRID_PAGE_SIZES'] = [
            ['NAME' => '10', 'VALUE' => '10'],
            ['NAME' => '20', 'VALUE' => '20'],
            ['NAME' => '50', 'VALUE' => '50'],
            ['NAME' => '100', 'VALUE' => '100'],
        ];
    }

    private function setFilter(): void
    {
        $this->filter = ['UF_IMPORT_FROM' => $this->arParams['TYPE']];
    }

    private function setSort(): void
    {
        //$sort = $this->gridOptions->GetSorting();
        $this->sort = ['UF_IMPORTFILE_DATE' => 'DESC'];
    }

    /**
     * Формирование данных для табличной части
     */
    private function setElements(): void
    {
        $count = (int)$this->navParams['nPageSize'];
        $offset = (int)$this->navParams['offset'];

        $filter = is_array($this->filter) ? $this->filter : [];
        $sort = is_array($this->sort) ? $this->sort : [];

        $resFiles = $this->getFilesListResource($count, $offset, $filter, $sort);
        if ($resFiles instanceof QueryResult) {
            try {
                $filesList = $resFiles->fetchAll();
                $filesCount = $resFiles->getCount();
            } catch (\Exception $e) {
                $filesList = [];
                $filesCount = 0;
            }
        } else {
            $filesList = [];
            $filesCount = 0;
        }

        $this->arResult['NAV_OBJECT']->setRecordCount($filesCount);
        $this->arResult['TOTAL_RECORDS_COUNT'] = $filesCount;

        $this->arResult['ELEMENTS_ROWS'] = [];

        $logPath = ImportLog::getLogFilesPath();

        foreach ($filesList as $file) {
            unset($logInfoFileURL);

            if (filesize(
                    Application::getDocumentRoot() . $logPath . '/' . $file['ORIGINAL_FILE_NAME'] . '_info.log'
                ) > 0) {
                $logInfoFileURL = '<a href="' . $logPath . '/' . $file['ORIGINAL_FILE_NAME'] . '_info.log" target="_blank"><span style="color: green;">Скачать инфо</span></a>';
            }
            if (filesize(
                    Application::getDocumentRoot() . $logPath . '/' . $file['ORIGINAL_FILE_NAME'] . '_error.log'
                ) > 0) {
                $logInfoFileURL .= '<br><a href="' . $logPath . '/' . $file['ORIGINAL_FILE_NAME'] . '_error.log" target="_blank"><span style="color: red;">Скачать ошибки</span></a>';
            }

            $data = [
                'ID' => $file['ID'],
                'FILE_DATE' => ($file['UF_IMPORTFILE_DATE'] instanceof Date) ? $file['UF_IMPORTFILE_DATE']->format('d.m.Y') : '-',
                'CONTENT_TYPE' => $file['UF_CONTENT_TYPE'],
                'UPLOAD_DATE' => ($file['UF_UPLOAD_DATE'] instanceof DateTime) ? $file['UF_UPLOAD_DATE']->format('d.m.Y H:i:s') : '-',
                'LAST_MODIFIED' => ($file['UF_LAST_MODIFIED'] instanceof DateTime) ? $file['UF_LAST_MODIFIED']->format('d.m.Y H:i:s') : '-',
                'IMPORT_DATE' => ($file['UF_IMPORT_DATE'] instanceof DateTime) ? $file['UF_IMPORT_DATE']->format('d.m.Y H:i:s') : '-',
                'IMPORT_RESULT' => empty($file['UF_IMPORT_RESULT']) ? '-' : $file['UF_IMPORT_RESULT'],
                'IMPORT_LOG_FILE' => (isset($logInfoFileURL) !== false) ? $logInfoFileURL : '-',
            ];

            $actions = [];

            if ($this->arResult['SHOW_ROW_ACTIONS_MENU'] === true) {
                $filePath = '/upload/' . $file['SUBDIR'] . '/' . $file['FILE_NAME'];

                $actions[] = [
                    'text' => 'Скачать XML-файл ',
                    'onclick' => 'openUri("' . $filePath . '")',
                    'default' => false,
                ];

                if (strtolower($file['UF_IMPORT_RESULT']) !== 'complete') {
                    $importLink = '/eks_eup_import/' . $this->arParams['TYPE'] . '/' . $file['UF_IMPORTFILE_MD5'] . '/';

                    $actions[] = [
                        'text' => 'Импорт данных',
                        'onclick' => 'if (confirm("Импортировать данные из файла в БД?")) openUri("' . $importLink . '")',
                        'default' => false,
                    ];
                } else {
                    $actions[] = [
                        'text' => 'Отметить файл как не установленный',
                        'onclick' => 'if (confirm("Отметить файл как не установленный?")) markAsNotInstalled("' . $this->arResult['GRID_ID'] . '", "' . $file['UF_IMPORTFILE_MD5'] . '")',
                        'default' => false,
                    ];
                }
            }

            $this->arResult['ELEMENTS_ROWS'][] = [
                'id' => $file['ID'],
                'data' => $data,
                'editable' => false,
                'actions' => $actions,
            ];
        }
    }

    /**
     * Выборка списка загруженных файлов
     *
     * @param $count
     * @param $offset
     * @param $filter
     * @param $sort
     * @return QueryResult|null
     */
    private function getFilesListResource($count, $offset, $filter, $sort): ?QueryResult
    {
        if (empty($sort)) {
            $sort = ['ID' => 'DESC'];
        }

        if ((int)$count === 0) {
            $count = 10;
        }

        try {
            $table = new ImportTable();
            $res = $table->hlblock::getList([
                'select' => [
                    '*',
                    'SUBDIR' => 'FILE.SUBDIR',
                    'FILE_NAME' => 'FILE.FILE_NAME',
                    'ORIGINAL_FILE_NAME' => 'FILE.ORIGINAL_NAME',
                ],
                'filter' => $filter,
                'order' => $sort,
                'limit' => $count,
                'offset' => $offset,
                'count_total' => true,
                'group' => ['ID'],
                'runtime' => [
                    new ReferenceField(
                        'FILE',
                        FileTable::getEntity(),
                        ['=ref.ID' => 'this.UF_IMPORTFILE_ID'],
                        ['join_type' => 'INNER']
                    ),
                ],
            ]);
        } catch (\Exception $e) {
            $res = null;
        }

        return $res;
    }

}