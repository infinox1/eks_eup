<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

\Bitrix\Main\Page\Asset::getInstance()->addJs('/bitrix/js/main/utils.js');
\Bitrix\Main\Page\Asset::getInstance()->addJs('/bitrix/js/lists/js/lists.min.js');
\Bitrix\Main\Page\Asset::getInstance()->addJs('/local/templates/ds_main/sib/js/jquery.min.js');

if ($arResult['success'] === false) {
    echo $arResult['message'];
} else {
    $APPLICATION->SetAdditionalCSS('/bitrix/css/main/grid/webform-button.css');

    $APPLICATION->IncludeComponent(
        'sib:main.ui.grid',
        'eks.eup.admin',
        [
            'GRID_ID' => $arResult['GRID_ID'],
            'COLUMNS' => $arResult['HEADERS'],
            'ROWS' => $arResult['ELEMENTS_ROWS'],
            'NAV_OBJECT' => $arResult['NAV_OBJECT'],
            'TOTAL_ROWS_COUNT' => $arResult['TOTAL_RECORDS_COUNT'],
            'PAGE_SIZES' => $arResult['GRID_PAGE_SIZES'],
            'AJAX_MODE' => 'Y',
            'AJAX_ID' => \CAjax::getComponentID('sib:main.ui.grid', 'eks.eup.admin', ''),
            'ENABLE_NEXT_PAGE' => true,
            'AJAX_OPTION_JUMP' => 'N',
            'SHOW_CHECK_ALL_CHECKBOXES' => false,
            'SHOW_ROW_CHECKBOXES' => false,
            'SHOW_ROW_ACTIONS_MENU' => $arResult['SHOW_ROW_ACTIONS_MENU'],
            'SHOW_GRID_SETTINGS_MENU' => false,
            'SHOW_NAVIGATION_PANEL' => true,
            'SHOW_PAGINATION' => true,
            'SHOW_SELECTED_COUNTER' => false,
            'SHOW_TOTAL_COUNTER' => true,
            'SHOW_PAGESIZE' => true,
            'SHOW_ACTION_PANEL' => false,
            'ALLOW_COLUMNS_SORT' => true,
            'ALLOW_COLUMNS_RESIZE' => true,
            'ALLOW_HORIZONTAL_SCROLL' => true,
            'ALLOW_SORT' => true,
            'ALLOW_PIN_HEADER' => true,
            'AJAX_OPTION_HISTORY' => true,
        ]
    );
}