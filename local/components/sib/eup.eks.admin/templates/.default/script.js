
function openUri(uri) {
    window.open(uri, '_blank');
}


function markAsNotInstalled(gridId, fileMD5) {

    jQuery.ajax({
        url: '/local/modules/sib.exchange.ekseup/ajax.php',
        type: 'POST',
        dataType: 'json',
        data: {
            'action': 'markAsNotInstalled',
            'md5': fileMD5,
        },

        success: function (response) {
            if (response.status == 'ok') {
                var reloadParams = {
                    clear_nav: 'Y',
                };

                var gridObject = BX.Main.gridManager.getById(gridId);

                if (gridObject.hasOwnProperty('instance')) {
                    gridObject.instance.reloadTable('POST', reloadParams);
                }

            } else if (response.status == 'error') {
                alert(response.message)
            }
        },

        error: function (jqXHR, exception) {
            console.log(jqXHR);
            if (jqXHR.status === 0) {
                alert('Ошибка сети');
            } else if (jqXHR.status == 404) {
                alert('Запрашиваемый ресурс не найден 404');
            } else if (jqXHR.status == 500) {
                alert('Внутренняя ошибка сервера 500');
            } else if (exception === 'parsererror') {
                alert('Ошибка обработки ответа сервера');
            } else if (exception === 'timeout') {
                alert('Time out error');
            } else if (exception === 'abort') {
                alert('Выполнение запроса прервано');
            } else {
                alert('Необрабатываемая ошибка ' + jqXHR.responseText);
            }
        }
    });
}

