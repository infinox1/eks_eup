<?php

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Application;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\DB\SqlQueryException;
use Bitrix\Main\Loader;
use Bitrix\Main\ObjectPropertyException;
use Bitrix\Main\SystemException;
use Bitrix\Main\UserTable;
use Sibintek\Actualizer\Exception\ActualizeException;
use Sibintek\Actualizer\OgUserActualizer;
use Sibintek\Exchange\EKSEUP\Import;
use Sibintek\Model\Common\ActionerTable;
use Sibintek\Model\Common\OPFTable;
use Sibintek\Model\Common\OUTable;
use Sibintek\Model\Common\PasportOGTable;
use Sibintek\Model\Common\PositionTable;
use Sibintek\Exchange\EKSEUP\XML_EUP_MembersOUTable;


require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

/*

Скрипт удален

*/