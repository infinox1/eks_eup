<?php

use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;

global $APPLICATION;

$moduleId = 'sib.exchange.ekseup';

$moduleRights = $APPLICATION->GetGroupRight($moduleId);

$request = Application::getInstance()->getContext()->getRequest();

$allOptions = [
    ['import_files_dir', 'Каталог для файлов обмена', ['text', '/upload/exchange.ekseup/external_files'], 'disabled'],
    ['log_dir', 'Каталог для логов импорта', ['text', '/upload/exchange.ekseup/import_log'], 'disabled'],
    ['keep_imported_files', 'Хранить импортированные в БД данные, суток', ['text', '30']],
    ['keep_not_imported_files', 'Хранить не импортированные в БД данные, суток', ['text', '10']],
    ['allowed_file_type', 'Разрешенный тип данных для этого стенда', ['selectbox', ['dev' => 'DEV', 'tst' => 'TST', 'qas' => 'QAS', 'PROD' => 'PROD'], 'dev']],
    ['remote_addr_dev', 'Адрес удаленного сервера для файла DEV', ['text', '10.10.10.11']],
    ['remote_addr_tst', 'Адрес удаленного сервера для файла TST', ['text', '10.10.10.12']],
    ['remote_addr_qas', 'Адрес удаленного сервера для файла QAS', ['text', '10.10.10.13']],
    ['remote_addr_prod', 'Адрес удаленного сервера для файла PROD', ['text', '10.10.10.14']],
];

if (($request->isPost() === true) && check_bitrix_sessid() && ($moduleRights >= 'R')) {
    if (!empty($request->get('default'))) {

        Option::delete($moduleId);

        foreach ($allOptions as $option) {

            $value = trim($option[2][1]);

            if (($option[2][0] === 'checkbox') && ($value !== 'Y')) {
                $value = 'N';
            } elseif (($option[2][0] === 'selectbox')) {
                $value = $option[2][2];
            }

            Option::set($moduleId, $option[0], $value);
        }

        CAdminMessage::showMessage(['TYPE' => 'OK', 'MESSAGE' => 'Установлены настройки по умолчанию']);
    } elseif (!empty($request->get('apply'))) {
        foreach ($allOptions as $option) {
            $value = trim($request->get($option[0]));

            if (($option[2][0] === 'checkbox') && ($value !== 'Y')) {
                $value = 'N';
            }

            if (!empty($value)) {
                Option::set($moduleId, $option[0], $value);
            }
        }

        CAdminMessage::showMessage(['TYPE' => 'OK', 'MESSAGE' => 'Настройки сохранены']);
    }
}

$aTabs = [
    ['DIV' => 'edit1', 'TAB' => 'Настройки', 'TITLE' => 'Основные настройки'],
];

$tabControl = new CAdminTabControl('tabControl', $aTabs);
$tabControl->Begin();

$tabControl->beginNextTab();
?>
    <form method="post" action="<?=sprintf('%s?mid=%s&lang=%s', $request->getRequestedPage(), $moduleId, LANGUAGE_ID) ?>">
        <?=bitrix_sessid_post()?>

        <tr class="heading">
            <td colspan="2">Общие настройки</td>
        </tr>

        <?php
        foreach ($allOptions as $option) {
            $currentValue = Option::get($moduleId, $option[0]); ?>

            <tr>
                <td width="40%"><label for="<?=$option[0]?>"><?=$option[1]?></label></td>
                <td width="60%" nowrap>
                    <?php if($option[2][0] === 'checkbox'):?>
                        <input type="checkbox" id="<?=$option[0]?>" name="<?=$option[0]?>" value="Y" <?=($currentValue === 'Y' ? 'checked' : '')?>>
                    <?php elseif($option[2][0] === 'text'):?>
                        <input type="text" size="<?=strlen($currentValue)?>" id="<?=$option[0]?>" name="<?=$option[0]?>" value="<?=$currentValue?>" <?=($option[3] === 'disabled' ? 'disabled' : '')?>>
                    <?php elseif($option[2][0] === 'selectbox'):?>
                        <select name="<?=htmlspecialcharsbx($option[0])?>">
                            <?
                            foreach ($option[2][1] as $key => $value)
                            {
                                ?><option value="<?= $key ?>"<?= ($key === $currentValue) ? " selected" : "" ?>><?= $value ?></option><?
                            }
                            ?>
                        </select>
                    <?php
                    endif; ?>
                </td>
            </tr>
            <?php
        }
        ?>

        <?php $tabControl->buttons(); ?>
        <input type="submit" name="apply" value="Применить" title="Сохранить изменения" <?=($moduleRights<'W') ? "disabled" : ''?> class="adm-btn-save bx-ui-loc-i-delete-all"/>
        <input type="submit" name="default" value="По умолчанию" title="Восстановить настройки по умолчанию" onclick="return confirm('Внимание! Все настройки будут перезаписаны значениями по умолчанию. Продолжить?')"/>
        <?php $tabControl->end(); ?>
    </form>

<?php
