<?php

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php');

$APPLICATION->SetTitle('Файлы обмена из ЕКС и ЕУП');

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php');

$arTabs = [
    ['DIV' => 'eks', 'TAB' => 'ЕКС', 'ICON' => '', 'TITLE' => 'Файлы обмена ЕКС'],
    ['DIV' => 'eup', 'TAB' => 'ЕУП', 'ICON' => '', 'TITLE' => 'Файлы обмена ЕУП'],
];

$tabControl = new CAdminTabControl('tabControl', $arTabs, true, true);
$tabControl->Begin();
$tabControl->BeginNextTab();
?>
    <tr>
        <td>
            <?php
            $APPLICATION->IncludeComponent(
                'sib:eup.eks.admin',
                '',
                [
                    'TYPE' => 'EKS',
                ]
            );
            ?>
        </td>
    </tr>
<?php
$tabControl->BeginNextTab();
?>
    <tr>
        <td>
            <?php
            $APPLICATION->IncludeComponent(
                'sib:eup.eks.admin',
                '',
                [
                    'TYPE' => 'EUP',
                ]
            );
            ?>
        </td>
    </tr>
<?php
$tabControl->End();

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php');