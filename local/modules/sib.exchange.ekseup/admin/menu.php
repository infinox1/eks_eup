<?php

$menu = [
    [
        'parent_menu' => 'global_menu_settings',
        'sort' => 400,
        'text' => 'Обмен с ЕКС и ЕУП',
        'title' => 'Меню',
        'url' => '/bitrix/admin/settings.php?lang=ru&mid=sib.exchange.ekseup&mid_menu=1',
        "icon" => "prototype_menu_icon",
        'items_id' => 'menu_references',
    ],
];

return $menu;
