<?php

namespace Sibintek\Exchange\EKSEUP;

use Bitrix\Highloadblock\DataManager;
use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\SystemException;
use Bitrix\Main\Type\DateTime;
use Sibintek\Exchange\EKSEUP\Exception as EKSEUPException;

class ImportTable extends DataManager
{

    private const table = 'sib_hl_exchange_ekseup';

    public $hlblock;

    public function __construct()
    {
        $hlblock = HighloadBlockTable::getList([
            'select' => ['*'],
            'filter' => ['=TABLE_NAME' => self::table],
            //'cache' => ['ttl' => 86400],
            'limit' => 1,
        ])->fetch();

        if ((int)$hlblock['ID'] === 0) {
            throw new EKSEUPException('Не найден HLB ' . self::table);
        }

        $this->hlblock = HighloadBlockTable::CompileEntity($hlblock)->getDataClass();
    }

    /**
     * @return array|array[]
     */
    public static function getMap()
    {
        return [
            'ID' => [
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
            ],
        ];
    }

    /**
     * @param array $parameters
     * @return array
     * @throws Exception
     */
    public static function getRecords(array $parameters = [])
    {
        try {
            $table = new self();
            $result = $table->hlblock::getList($parameters)->fetchAll();
        } catch (\Exception $e) {
            throw new EKSEUPException('Ошибка выборки из таблицы EKSEUPTable: ' . $e->getMessage());
        }

        return $result;
    }

    /**
     * @param array $fields
     * @return int
     * @throws Exception
     */
    public function addRecord(array $fields): int
    {
        $fields['UF_UPLOAD_DATE'] = new DateTime();
        $fields['UF_LAST_MODIFIED'] = new DateTime();

        try {
            $result = $this->hlblock::add($fields);

            if ($result->isSuccess() !== true) {
                throw new EKSEUPException(
                    'Ошибка добавления записи в таблицу EKSEUPTable: ' . implode('; ', $result->getErrorMessages())
                );
            }

            $id = (int)$result->getId();
        } catch (\Exception $e) {
            throw new EKSEUPException('Ошибка добавления записи в таблицу EKSEUPTable: ' . $e->getMessage());
        }

        if ($id === 0) {
            throw new EKSEUPException('Ошибка добавления записи в таблицу EKSEUPTable, возвращен ID=0');
        }

        return $id;
    }

    /**
     * @param $id
     * @param array $fields
     * @throws Exception
     */
    public function updateRecord($id, array $fields)
    {
        $fields['UF_LAST_MODIFIED'] = new DateTime();

        try {
            $result = $this->hlblock::update($id, $fields);

            if ($result->isSuccess() !== true) {
                throw new EKSEUPException(
                    'Ошибка обновления записи ID=' . $id . ' в таблице KSEDTable: ' . implode(
                        '; ',
                        $result->getErrorMessages()
                    )
                );
            }
        } catch (\Exception $e) {
            throw new EKSEUPException(
                'Ошибка обновления записи ID=' . $id . ' в таблице KSEDTable: ' . $e->getMessage()
            );
        }
    }


    /**
     * @param $id
     * @param array $fields
     * @throws Exception
     */
    public function deleteRecord($id)
    {
        try {
            $uploadFile = $this->hlblock::getList([
                'select' => ['ID', 'FILE_ID' => 'UF_IMPORTFILE_ID'],
                'filter' => ['=ID' => $id],
                'limit' => 1,
            ])->fetch();

            if ((int)$uploadFile['FILE_ID'] === 0) {
                return;
            }

            \CFile::Delete($uploadFile['FILE_ID']);

            $result = $this->hlblock::delete($uploadFile['ID']);

            if ($result->isSuccess() !== true) {
                throw new EKSEUPException('Ошибка удаления записи ID=' . $uploadFile['ID'] . ' в таблице KSEDTable' . $result->getErrorMessages());
            }
        } catch (\Exception $e) {
            throw new EKSEUPException('Ошибка удаления записи ID=' . $uploadFile['ID'] . ' в таблице KSEDTable: ' . $e->getMessage());
        }
    }


}
