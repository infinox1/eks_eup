<?php

namespace Sibintek\Exchange\EKSEUP;

use Bitrix\Main\ORM\Data\DataManager;
use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Main\ORM\Fields\TextField;
use Bitrix\Main\ORM\Fields\BooleanField;

class XML_EUP_MembersOUTable extends DataManager
{

    public static function getTableName(): string
    {
        return 'sib_tmp_ekseup_membersou';
    }

    /**
     * @return \Bitrix\Main\ORM\Objectify\EntityObject|string
     */
    public static function getObjectClass()
    {
        return XML_EUP_MembersOUTable::class;
    }

    public static function getMap(): array
    {
        return [
            new IntegerField(
                'ID',
                [
                    'title' => 'ID',
                    'primary' => true,
                    'autocomplete' => true,
                ]
            ),
            new IntegerField(
                'OG_ID',
                [
                    'title' => 'OG_ID',
                ]
            ),
            new BooleanField(
                'IN_NS',
                [
                    'title' => 'NS',
                ]
            ),
            new BooleanField(
                'IN_SD',
                [
                    'title' => 'SD',
                ]
            ),
            new IntegerField(
                'OVERALL_COUNT',
                [
                    'title' => 'OVERALL_COUNT',
                ]
            ),
            new IntegerField(
                'RN_COUNT',
                [
                    'title' => 'RN_COUNT',
                ]
            ),
        ];
    }

}