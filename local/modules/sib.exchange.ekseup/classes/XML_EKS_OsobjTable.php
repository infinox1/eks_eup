<?php

namespace Sibintek\Exchange\EKSEUP;

use Bitrix\Main\Entity;
use Bitrix\Main\ORM\Data\DataManager;
use Bitrix\Main\ORM\Fields\IntegerField;
use Bitrix\Main\ORM\Fields\TextField;

class XML_EKS_OsobjTable extends DataManager
{

    public static function getTableName(): string
    {
        return 'sib_tmp_ekseup_osobj';
    }

    /**
     * @return \Bitrix\Main\ORM\Objectify\EntityObject|string
     */
    public static function getObjectClass()
    {
        return XML_EKS_OsobjTable::class;
    }

    public static function getMap(): array
    {
        return [
            new IntegerField(
                'ID',
                [
                    'title' => 'ID',
                    'primary' => true,
                    'autocomplete' => true,
                ]
            ),
            new TextField(
                'BEGDA',
                [
                    'title' => 'BEGDA',
                ]
            ),
            new TextField(
                'ENDDA',
                [
                    'title' => 'ENDDA',
                ]
            ),
            new TextField(
                'BP',
                [
                    'title' => 'BP',
                ]
            ),
            new TextField(
                'BUKRS',
                [
                    'title' => 'BUKRS',
                ]
            ),
            new TextField(
                'ISBOSS',
                [
                    'title' => 'ISBOSS',
                ]
            ),
            new TextField(
                'ISSSPBOSS',
                [
                    'title' => 'ISSSPBOSS',
                ]
            ),
            new TextField(
                'ND',
                [
                    'title' => 'ND',
                ]
            ),
            new TextField(
                'OBJID',
                [
                    'title' => 'OBJID',
                ]
            ),
            new TextField(
                'OPERA',
                [
                    'title' => 'OPERA',
                ]
            ),
            new TextField(
                'OTYPE',
                [
                    'title' => 'OTYPE',
                ]
            ),
            new TextField(
                'OWNER',
                [
                    'title' => 'OWNER',
                ]
            ),
            new TextField(
                'ORGEH2',
                [
                    'title' => 'ORGEH2',
                ]
            ),
            new TextField(
                'TXTRU',
                [
                    'title' => 'TXTRU',
                ]
            ),
            new IntegerField(
                'PARENT_SECTION_ID',
                [
                    'title' => 'PARENT_SECTION_ID',
                    'nullable' => true,
                ]
            ),
        ];
    }

}
