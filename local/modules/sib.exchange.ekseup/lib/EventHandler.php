<?php

namespace Sibintek\Exchange\EKSEUP;

use Bitrix\Iblock\Model\Section;


class EventHandler
{

    /**
     * Проверка изменения руководителя департамента из админки
     * Если руководитель изменен - выставляется соответствующий флаг
     *
     * @param array $fields
     * @return bool
     */
    public static function OnBeforeIBlockSectionUpdateHandler(array &$fields): bool
    {
        global $APPLICATION;

        if ((int)$fields['IBLOCK_ID'] !== IB_departments) {
            return true;
        }

        // Если изменение департамента происходит через API, то проверку флага не производим (ориентируемся на IPROPERTY_TEMPLATES)
        if(isset($fields['IPROPERTY_TEMPLATES']) !== true) {
            return true;
        }

        // Если у этого департамента руководитель сейчас выставлен вручную, то руководителя не проверяем
        if ((int)$fields['UF_HEAD_MANUALLY'] === 1) {
            return true;
        }

        try {
            $entity = Section::compileEntityByIblock(IB_departments);
            $section = $entity::getList([
                'select' => ['ID', 'UF_HEAD', 'UF_HEAD_MANUALLY'],
                'filter' => ['ID' => $fields['ID']],
                'limit' => 1,
            ])->fetch();

            // Если сохраненный руководитель департамента отличается от нового, то добавляем флаг
            if ((int)$section['UF_HEAD'] !== (int)$fields['UF_HEAD']) {
                $fields['UF_HEAD_MANUALLY'] = 1;
            }

        } catch (\Exception $e) {
        }

        return true;
    }

}
