<?php

namespace Sibintek\Exchange\EKSEUP;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Type\Date;
use Bitrix\Main\Application;
use Sibintek\Exchange\EKSEUP\ImportTable;

/**
 * Class FileHandler
 *
 * Сохранение штатными средствами файлов импорта из /upload/exchange.ekseup/external_files
 * Проверка на соответствие установленному типу данных (prod, dev, ...)
 *
 * @package Sibintek\Exchange\EKSEUP
 */
class FileHandler
{

    private const MODULE_ID = 'sib.exchange.ekseup';

    /**
     * Проверка и импорт новых файлов из /upload/exchange.ekseup/external_files
     *
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     */
    public static function checkNewFiles(): void
    {
        $externalFilesDir = Application::getDocumentRoot() . Option::get(self::MODULE_ID, 'import_files_dir');
        if (is_dir($externalFilesDir) !== true) {
            return;
        }

        $allowedFileType = strtolower(Option::get(self::MODULE_ID, 'allowed_file_type'));
        if ($allowedFileType === '') {
            return;
        }

        foreach (scandir($externalFilesDir) as $fileName) {
            if (preg_match('/^(eks|eup)/ui', $fileName) !== 1) {
                continue;
            }

            // Файлы с недопустимым типом для данного стенда переименовываем
            if (preg_match('/\S+_' . $allowedFileType . '_\S+/ui', $fileName) !== 1) {
                $newName = $externalFilesDir . '/ERROR_TYPE_' . $fileName;
                @rename($externalFilesDir . '/' . $fileName, $newName);

                continue;
            }

            // TODO Проверка MD5 текущего файла для предотвращения повторной загрузки файла импорта с точно таким же содержимым

            $fileArray = \CFile::MakeFileArray($externalFilesDir . '/' . $fileName);
            $fileArray['MODULE_ID'] = self::MODULE_ID;
            $fileId = \CFile::SaveFile($fileArray, 'import_ekseup', true, false);

            if ((int)$fileId === 0) {
                // TODO Переименовываем файл ???
                continue;
            }

            preg_match('/\d{4}_\d{2}_\d{2}/u', $fileName, $fileDate);
            if (isset($fileDate[0]) === true) {
                $xmlDate = new Date($fileDate[0], 'Y_m_d');
            } else {
                $xmlDate = Date::createFromTimestamp(filemtime($externalFilesDir . '/' . $fileName));
            }

            $fields = [
                'UF_IMPORTFILE_ID' => $fileId, //$fileArray,
                'UF_IMPORTFILE_DATE' => $xmlDate,
                'UF_IMPORTFILE_MD5' => md5($externalFilesDir . '/' . $fileName),
                'UF_CONTENT_TYPE' => $allowedFileType,
                'UF_IMPORT_FROM' => substr($fileName, 0, 3),
            ];

            try {
                $importTable = new ImportTable();

                $importTable->addRecord($fields);

                @unlink($externalFilesDir . '/' . $fileName);
            } catch (\Exception $e) {
                // TODO отправка почтового сообщения ???
            }
        }
    }

    /**
     * Копирование сегодняшних файлов с данными ЕКС и КС ЕУП с шины
     *
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     */
    public static function getRemoteFiles(): void
    {
        $externalFilesDir = Application::getDocumentRoot() . Option::get(self::MODULE_ID, 'import_files_dir');
        if (is_dir($externalFilesDir) !== true) {
            return;
        }

        $logDir = Option::get(self::MODULE_ID, 'log_dir');
        $logDir === '' ? $logDir = '/upload/exchange.ekseup/import_log' : null;
        $log = Application::getDocumentRoot() . $logDir . '/remote_copy.log';

        $allowedFileType = strtolower(Option::get(self::MODULE_ID, 'allowed_file_type'));
        if($allowedFileType === '') {
            return;
        }

        $fileSuffix = '_' . $allowedFileType . '_' . date('Y_m_d') . '.xml';

        $remoteAddress = Option::get(self::MODULE_ID, 'remote_addr_' . $allowedFileType);
        stripos($remoteAddress, 'http:') !== 0 ? $remoteAddress = 'http://' . $remoteAddress : '';

        $localFile = $externalFilesDir . '/eks' . $fileSuffix;
        $remoteFile = $remoteAddress . '/eks/eks' . $fileSuffix;

        file_put_contents($log, date('Y-m-d H:i:s') . 'Trying to copy ' . $remoteFile . ' -> ' . $localFile . ' : ', FILE_APPEND);

        if (copy($remoteFile, $localFile) !== false) {
            file_put_contents($log, 'OK' . chr(13), FILE_APPEND);
        } else {
            file_put_contents($log, 'ERROR' . chr(13), FILE_APPEND);
        }

        $localFile = $externalFilesDir . '/eup' . $fileSuffix;
        $remoteFile = $remoteAddress . '/eup/eup' . $fileSuffix;

        file_put_contents($log, date('Y-m-d H:i:s') . 'Trying to copy ' . $remoteFile . ' -> ' . $localFile . ' : ', FILE_APPEND);

        if (copy($remoteFile, $localFile) !== false) {
            file_put_contents($log, 'OK' . chr(13), FILE_APPEND);
        } else {
            file_put_contents($log, 'ERROR' . chr(13), FILE_APPEND);
        }

        file_put_contents($log, chr(13), FILE_APPEND);
    }


    /**
     * Удаление устаревших записей и файлов импорта
     *
     * @throws \Bitrix\Main\ArgumentNullException
     * @throws \Bitrix\Main\ArgumentOutOfRangeException
     */
    public static function deleteDeprecatedFiles(): void
    {
        $recordsToDelete = [];
        $importTable = new ImportTable();

        $depth = (int)Option::get(self::MODULE_ID, 'keep_not_imported_files');
        if ($depth > 0) {

            $deleteDate = new Date();
            $deleteDate->add('-' . $depth . 'd');

            try {
                $elements = $importTable::getRecords([
                    'select' => ['ID'],
                    'filter' => ['<UF_IMPORTFILE_DATE' => $deleteDate, 'UF_IMPORT_DATE' => null],
                ]);
                foreach ($elements as $element) {
                    $recordsToDelete[] = (int)$element['ID'];
                }
            } catch (\Exception $e) {
            }
        }

        $depth = (int)Option::get(self::MODULE_ID, 'keep_imported_files');
        if ($depth > 0) {

            $deleteDate = new Date();
            $deleteDate->add('-' . $depth . 'd');

            try {
                $elements = $importTable::getRecords([
                    'select' => ['ID'],
                    'filter' => ['<UF_IMPORT_DATE' => $deleteDate, '!UF_IMPORT_DATE' => null],
                ]);
                foreach ($elements as $element) {
                    $recordsToDelete[] = (int)$element['ID'];
                }
            } catch (\Exception $e) {
            }
        }

        if (count($recordsToDelete) === 0) {
            return;
        }

        foreach ($recordsToDelete as $recordId) {
            try {
                $importTable->deleteRecord($recordId);
            } catch (\Exception $e) {
            }
        }
    }


    public static function checkNewFilesAgent(): string
    {
        try {
            self::checkNewFiles();
        } catch (\Exception $e) {
        }

        return '\\Sibintek\\Exchange\\EKSEUP\\FileHandler::checkNewFilesAgent();';
    }


    public static function getRemoteFilesAgent(): string
    {
        try {
            self::getRemoteFiles();
            self::deleteDeprecatedFiles();
        } catch (\Exception $e) {
        }

        return '\\Sibintek\\Exchange\\EKSEUP\\FileHandler::getRemoteFilesAgent();';
    }

}
