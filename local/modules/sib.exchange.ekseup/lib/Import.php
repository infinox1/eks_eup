<?php

namespace Sibintek\Exchange\EKSEUP;

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\ORM\Entity as ORMEntity;
use Bitrix\Main\Entity\ReferenceField;
use Bitrix\Main\FileTable;
use Bitrix\Main\Type\DateTime;
use Sibintek\Exchange\EKSEUP\ImportTable;
use Sibintek\Exchange\EKSEUP\Exception as EKSEUPException;
use Sibintek\Exchange\EKSEUP\ImportLog;

class Import
{

    /** @var ORMEntity */
    private $table; // Сущность таблицы sib_hl_exchange_ekseup

    /** @var integer */
    private $id; // текущий ID записи в таблице $table sib_hl_exchange_ekseup

    /** @var \SimpleXMLElement */
    private $XMLContent; // Объект xml-файла с данными импорта

    /** @var \Bitrix\Main\Type\Date */
    private $fileDate; // Дата файла с данными импорта

    /** @var string */
    private $importFileOriginalName; // Оригинальное имя файла с данными импорта формата eks_dev_2023_10_01.xml

    /** @var string */
    private $filePath; // Путь к файлу с данными импорта

    /** @var \Sibintek\Exchange\EKSEUP\ImportLog */
    public $Log; // Класс логирования импорта

    public function __construct()
    {
        // Инициализация таблицы ImportTable
        try {
            Loader::includeModule('highloadblock');

            $this->table = new ImportTable();

            if (($this->table::getEntity() instanceof ORMEntity) !== true) {
                throw new EKSEUPException('Ошибка инициализации таблицы ImportTable');
            }
        } catch (\Exception $e) {
            throw new EKSEUPException('Ошибка инициализации таблицы ImportTable: ' . $e->getMessage());
        }
    }

    /**
     * Привязка экземпляра класса к одной записи в таблице ImportTable по MD5 файла импорта
     *
     * @param string $md5
     * @throws Exception
     */
    public function setImportFileByMD5(string $md5): void
    {
        try {
            $rec = $this->table::getRecords([
                'select' => [
                    'ID',
                    'UF_IMPORTFILE_ID',
                    'UF_IMPORTFILE_DATE',
                    'SUBDIR' => 'FILE.SUBDIR',
                    'FILE_NAME' => 'FILE.FILE_NAME',
                    'ORIGINAL_FILE_NAME' => 'FILE.ORIGINAL_NAME',
                ],
                'filter' => ['=UF_IMPORTFILE_MD5' => $md5],
                'runtime' => [
                    new ReferenceField(
                        'FILE',
                        FileTable::getEntity(),
                        ['=ref.ID' => 'this.UF_IMPORTFILE_ID'],
                        ['join_type' => 'INNER']
                    ),
                ],
                'limit' => 1,
            ]);
        } catch (\Exception $e) {
            throw new EKSEUPException('Ошибка выборки записи с файлом обмена ' . $e->getMessage());
        }

        $fileFullPath = Application::getDocumentRoot() . '/upload/' . $rec[0]['SUBDIR'] . '/' . $rec[0]['FILE_NAME'];

        if (file_exists($fileFullPath) !== true) {
            throw new EKSEUPException('Файл с данными удален или не читается');
        }

        $this->filePath = $fileFullPath;

        libxml_use_internal_errors(true);

        $this->XMLContent = simplexml_load_string(file_get_contents($fileFullPath));

        if (!$this->XMLContent) {
            $errMsg = '';

            foreach (libxml_get_errors() as $err) {
                $errMsg .= $err->message . ' в строке ' . $err->line . '; ';
            }

            throw new EKSEUPException('Ошибка разбора XML из файла [' . $fileFullPath . ']: [' . $errMsg . ']');
        }

        $this->id = (int)$rec[0]['ID'];

        $this->fileDate = $rec[0]['UF_IMPORTFILE_DATE'];

        $this->importFileOriginalName = $rec[0]['ORIGINAL_FILE_NAME'];

        $this->Log = new ImportLog();
        $this->Log->setImportFileName($this->importFileOriginalName);
    }

    /**
     * Возвращает полный путь к XML-файлу импорта, загруженному ранее в /upload/import_ekseup
     *
     * @return string
     */
    public function getXMLFilePath(): string
    {
        return $this->filePath;
    }

    /**
     * Возвращает объект SimpleXMLElement
     *
     * @return \SimpleXMLElement
     */
    public function getXMLContent(): \SimpleXMLElement
    {
        return $this->XMLContent;
    }

    /**
     * Возвращает дату формирования файла импорта на 117 или 36 шине
     *
     * @return \Bitrix\Main\Type\Date
     */
    public function getXMLFileDate(): \Bitrix\Main\Type\Date
    {
        return $this->fileDate;
    }

    /**
     * Возвращает оригинальное имя файла формата eup_prod_2023_11_08.xml
     *
     * @return string
     */
    public function getImportFileOriginalName(): string
    {
        return $this->importFileOriginalName;
    }

    /**
     * Пишет в таблицу ImportTable текущий этап импорта (step в eks.php и eup.php)
     *
     * @param string $stage
     */
    public function setCurrentImportStage(string $stage): void
    {
        $fields = [
            'UF_IMPORT_RESULT' => trim($stage),
        ];

        if (strtolower($stage) === 'complete') {
            $fields['UF_IMPORT_DATE'] = new DateTime();
        }

        try {
            $this->table->updateRecord($this->id, $fields);
        } catch (EKSEUPException $e) {
        }
    }

    /**
     * Отмечает текущий импорт как не установленный для возможности прогрузить данные еще раз
     */
    public function markAsNotInstalled(): void
    {
        $fields = [
            'UF_IMPORT_DATE' => null,
            'UF_IMPORT_RESULT' => null,
        ];

        try {
            $this->table->updateRecord($this->id, $fields);
            $this->Log->truncateLog();
        } catch (EKSEUPException $e) {
        }
    }


}