<?php

namespace Sibintek\Exchange\EKSEUP;

use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;

class ImportLog
{
    private const MODULE_ID = 'sib.exchange.ekseup';

    /** @var string */
    private $fileInfo; // Имя файла с инфо-логом формата eup_prod_2023_11_08.xml_info.log , привязан к конкретному файлу импорта и типу его содержимого

    /** @var string */
    private $fileError; // Имя файла с логом ошибок формата eup_prod_2023_11_08.xml_error.log , привязан к конкретному файлу импорта и типу его содержимого

    /**
     * Возвращает каталог для логов в upload
     *
     * @return string
     */
    public static function getLogFilesPath(): string
    {
        $logDir = Option::get(self::MODULE_ID, 'log_dir');
        if ($logDir === '') {
            $logDir = '/upload/exchange.ekseup/import_log';
        }
        return $logDir;
    }

    /**
     * Устанавливает имена файлов для обоих логов - инфо и с ошибками
     *
     * @param string $fileName
     */
    public function setImportFileName($fileName = ''): void
    {
        $fileName = trim($fileName);
        if ($fileName === '') {
            $fileName = 'unknown_' . date('Y_M_D_H_i_s');
        }

        $this->fileInfo = Application::getDocumentRoot() . self::getLogFilesPath() . '/' . $fileName . '_info.log';
        if (file_exists($this->fileInfo) !== true) {
            file_put_contents($this->fileInfo, '');
        }

        $this->fileError = Application::getDocumentRoot() . self::getLogFilesPath() . '/' . $fileName . '_error.log';
        if (file_exists($this->fileError) !== true) {
            file_put_contents($this->fileError, '');
        }
    }

    /**
     * Обнуление обеих файлов лога в случае повторной загрузки данных
     */
    public function truncateLog(): void
    {
        file_put_contents($this->fileInfo, '');
        file_put_contents($this->fileError, '');
    }

    /**
     * @param $message
     */
    public function addInfoMessage($message): void
    {
        $this->addMessage($message, $this->fileInfo);
    }

    /**
     * @param $message
     */
    public function addErrorMessage($message): void
    {
        $this->addMessage($message, $this->fileError);
    }

    /**
     * @param mixed $message
     * @param string $file
     */
    private function addMessage($message, $file): void
    {
        if (is_array($message) === true) {
            $writableMessage = print_r($message, true);
        } elseif (is_object($message) === true) {
            $writableMessage = 'CLASS=' . get_class($message) . ' OBJECT=' . print_r($message, true);
        } elseif (is_null($message) === true) {
            $writableMessage = '<NULL>';
        } else {
            $writableMessage = (string)$message;
        }

        file_put_contents($file, date('Y-m-d H:i:s') . "\t" . $writableMessage . chr(10), FILE_APPEND);
    }

}