<?php

define("BX_COMPRESSION_DISABLED", true);
define("CUSTOM_COMPRESSION_DISABLED", true);
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS", true);
define("STOP_STATISTICS", true);
define("NO_AGENT_STATISTIC", true);

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Sibintek\Exchange\EKSEUP\Import;
use Sibintek\Exchange\EKSEUP\Exception as EKSEUPException;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

/*
 * Обработка запросов из main.ui.grid со списком файлов в админке
 *
 */

$request = Application::getInstance()->getContext()->getRequest();

$action = $request->get('action');

if ($action === 'markAsNotInstalled') {
    $result = [];

    try {
        Loader::includeModule('sib.exchange.ekseup');

        $import = new Import();
        $import->setImportFileByMD5($request->get('md5'));
        $import->markAsNotInstalled();
        $import->Log->truncateLog();

        $result = [
            'status' => 'ok',
        ];
    } catch (EKSEUPException $e) {
        $result = [
            'status' => 'error',
            'message' => 'Ошибка EKSEUPException: ' . $e->getMessage(),
        ];
    }

    echo json_encode($result);
    die;
} else {
    $result = [
        'status' => 'error',
        'message' => 'Не определено действие',
    ];
    echo json_encode($result);
    die;
}
