<?php

\Bitrix\Main\Loader::registerAutoLoadClasses('sib.exchange.ekseup', [

    // Внутренние классы
    'Sibintek\\Exchange\\EKSEUP\\ImportTable' => 'classes/ImportTable.php',

    // Временные таблицы для первоначального импорта данных из XML-файлов
    'Sibintek\\Exchange\\EKSEUP\\XML_EKS_OsobjTable' => 'classes/XML_EKS_OsobjTable.php',
    'Sibintek\\Exchange\\EKSEUP\\XML_EUP_MembersOUTable' => 'classes/XML_EUP_MembersOUTable.php',

    // Обработчики событий
    //'//Sibintek\\Exchange\\EKSEUP\\EventHandler' => 'lib/EventHandler.php',

    // Внешние методы модуля
    'Sibintek\\Exchange\\EKSEUP\\FileHandler' => 'lib/FileHandler.php',
    'Sibintek\\Exchange\\EKSEUP\\EventHandler' => 'lib/EventHandler.php',
    'Sibintek\\Exchange\\EKSEUP\\Import' => 'lib/Import.php',
    'Sibintek\\Exchange\\EKSEUP\\ImportLog' => 'lib/ImportLog.php',
    'Sibintek\\Exchange\\EKSEUP\\Exception' => 'lib/Exception.php',
]);
