<?php

use Bitrix\Main\Loader;
use Bitrix\Main\SystemException;
use Bitrix\Highloadblock\HighloadBlockTable;
use Bitrix\Highloadblock\HighloadBlockLangTable;

/**
 * Class migrationHelper реализует миграции HL-блоков при установке и удалении модуля
 * Все миграции хранятся в /install/db, имя файла должно совпадать с именем класса миграции (maintable.php => class maintable{})
 */
class migrationHelper
{

    public function __construct()
    {
        if (!Loader::includeModule('highloadblock')) {
            throw new SystemException('Ошибка подключения модуля highloadblock');
        }
    }

    /**
     * Создание HL-блока
     *
     * @param $fields
     * @throws SystemException
     */
    public function saveHlblock($fields)
    {
        $fields['NAME'] = ucfirst($fields['NAME']);

        if ($this->getHlblockIDByName($fields['NAME']) > 0) {
            throw new SystemException(
                'Ошибка создания highload блока ' . $fields['NAME'] . ': блок с таким же наименованием уже есть'
            );
        }

        $lang = [];
        if (isset($fields['LANG'])) {
            $lang = $fields['LANG'];
            unset($fields['LANG']);
        }

        try {
            $result = HighloadBlockTable::add($fields);
        } catch (\Exception $e) {
            throw new SystemException($e->getMessage());
        }

        if ($result->isSuccess() !== true) {
            throw new SystemException(
                'Ошибка создания highload блока ' . $fields['NAME'] . ': ' . implode('; ', $result->getErrorMessages())
            );
        }

        foreach ($lang as $lid => $item) {
            if (!empty($item['NAME'])) {
                try {
                    HighloadBlockLangTable::add([
                        'ID' => $result->getId(),
                        'LID' => $lid,
                        'NAME' => $item['NAME'],
                    ]);
                } catch (\Exception $e) {
                    throw new SystemException($e->getMessage());
                }
            }
        }
    }

    /**
     * Удаление HL-блока с заданным наименованием
     *
     * @param string $name
     * @throws SystemException
     */
    public function deleteHlblock($name = ''): void
    {
        $id = $this->getHlblockIDByName($name);

        if ($id === 0) {
            throw new SystemException('Ошибка удаления highload-блока ' . $name . ': такого блока не существует');
        }

        $result = HighloadBlockTable::delete($id);

        if ($result->isSuccess() !== true) {
            throw new SystemException('Ошибка удаления highload-блока ' . $name . ': ' . $result->getErrorMessages());
        }
    }

    /**
     * Сохранение пользовательского поля к HL-блоку
     *
     * @param array $fields
     * @throws SystemException
     */
    public function saveUserTypeEntity($fields = []): void
    {
        global $APPLICATION;

        $default = [
            "ENTITY_ID" => '',
            "FIELD_NAME" => '',
            "USER_TYPE_ID" => '',
            "XML_ID" => '',
            "SORT" => 500,
            "MULTIPLE" => 'N',
            "MANDATORY" => 'N',
            "SHOW_FILTER" => 'I',
            "SHOW_IN_LIST" => '',
            "EDIT_IN_LIST" => '',
            "IS_SEARCHABLE" => '',
            "SETTINGS" => [],
            "EDIT_FORM_LABEL" => ['ru' => '', 'en' => ''],
            "LIST_COLUMN_LABEL" => ['ru' => '', 'en' => ''],
            "LIST_FILTER_LABEL" => ['ru' => '', 'en' => ''],
            "ERROR_MESSAGE" => '',
            "HELP_MESSAGE" => '',
        ];

        $fields = array_replace_recursive($default, $fields);

        // Замена наименования HL-блока на его ID
        $hlblockName = str_replace('HLBLOCK_', '', $fields['ENTITY_ID']);
        $hlblockId = $this->getHlblockIDByName($hlblockName);

        if ($hlblockId === 0) {
            throw new SystemException(
                'Ошибка создания пользовательского поля ' . $fields['FIELD_NAME'] . ': не найден highload-блок ' . $hlblockName
            );
        }

        $fields['ENTITY_ID'] = 'HLBLOCK_' . $hlblockId;

        $enums = [];
        if (isset($fields['ENUM_VALUES'])) {
            $enums = $fields['ENUM_VALUES'];
            unset($fields['ENUM_VALUES']);
        }

        $obUserField = new CUserTypeEntity;
        $userFieldId = $obUserField->Add($fields);

        if ((int)$userFieldId === 0) {
            throw new SystemException(
                'Ошибка создания пользовательского поля ' . $fields['FIELD_NAME'] . ': ' . $APPLICATION->GetException(
                )->GetString()
            );
        }

        if ($userFieldId && $fields['USER_TYPE_ID'] === 'enumeration') {
            $index = 0;
            $updates = [];
            foreach ($enums as $newenum) {
                $updates['n' . $index++] = $newenum;
            }

            $obEnum = new CUserFieldEnum();
            $result = $obEnum->SetEnumValues($userFieldId, $updates);

            // TODO что делать при ошибке создания списка значений?
            if ((bool)$result === false) {
                //throw new SystemException('Ошибка создания пользовательского поля ' . $fields['FIELD_NAME'] . ': ' . $APPLICATION->GetException()->GetString());
            }
        }
    }

    /**
     * Возвращает ID HL-блока по его наименованию или 0, если блок не найден
     *
     * @param string $name
     * @return int
     */
    private function getHlblockIDByName($name = ''): int
    {
        try {
            $hlblock = HighloadBlockTable::getList([
                'select' => ['ID'],
                'filter' => ['=NAME' => $name],
                'limit' => 1,
            ])->fetch();
        } catch (\Exception $e) {
            return 0;
        }

        return (int)$hlblock['ID'];
    }

}