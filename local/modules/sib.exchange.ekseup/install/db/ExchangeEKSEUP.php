<?php

class ExchangeEKSEUP
{

    private const HLBLOCK_NAME = 'ExchangeEKSEUP';
    private const TABLE_NAME = 'sib_hl_exchange_ekseup';

    public static function up()
    {
        $helper = new migrationHelper();

        $helper->saveHlblock([
            'NAME' => self::HLBLOCK_NAME,
            'TABLE_NAME' => self::TABLE_NAME,
            'LANG' => [
                'ru' => [
                    'NAME' => 'Импорт из ЕКС и ЕУП',
                ],
            ],
        ]);

        $helper->saveUserTypeEntity([
            'ENTITY_ID' => 'HLBLOCK_' . self::HLBLOCK_NAME,
            'FIELD_NAME' => 'UF_IMPORTFILE_DATE',
            'USER_TYPE_ID' => 'date',
            'XML_ID' => '',
            'SORT' => '100',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'EDIT_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'DEFAULT_VALUE' => [
                    'TYPE' => 'NOW',
                    'VALUE' => '',
                ],
                'USE_SECOND' => 'Y',
            ],
            'EDIT_FORM_LABEL' => [
                'en' => '',
                'ru' => 'ДатаВремя создания файла импорта',
            ],
            'LIST_COLUMN_LABEL' => [
                'en' => '',
                'ru' => '',
            ],
            'LIST_FILTER_LABEL' => [
                'en' => '',
                'ru' => '',
            ],
            'ERROR_MESSAGE' => [
                'en' => '',
                'ru' => '',
            ],
            'HELP_MESSAGE' => [
                'en' => '',
                'ru' => '',
            ],
        ]);

        $helper->saveUserTypeEntity([
            'ENTITY_ID' => 'HLBLOCK_' . self::HLBLOCK_NAME,
            'FIELD_NAME' => 'UF_IMPORTFILE_ID',
            'USER_TYPE_ID' => 'integer',
            'XML_ID' => '',
            'SORT' => '100',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'EDIT_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'SIZE' => 5,
                'MIN_VALUE' => 0,
                'MAX_VALUE' => 0,
                'DEFAULT_VALUE' => '',
            ],
            'EDIT_FORM_LABEL' => [
                'en' => '',
                'ru' => 'ID загруженного файла с данными',
            ],
            'LIST_COLUMN_LABEL' => [
                'en' => '',
                'ru' => 'ID загруженного файла с данными',
            ],
            'LIST_FILTER_LABEL' => [
                'en' => '',
                'ru' => '',
            ],
            'ERROR_MESSAGE' => [
                'en' => '',
                'ru' => '',
            ],
            'HELP_MESSAGE' => [
                'en' => '',
                'ru' => '',
            ],
        ]);

        $helper->saveUserTypeEntity([
            'ENTITY_ID' => 'HLBLOCK_' . self::HLBLOCK_NAME,
            'FIELD_NAME' => 'UF_IMPORTFILE_MD5',
            'USER_TYPE_ID' => 'string',
            'XML_ID' => '',
            'SORT' => '100',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'EDIT_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'SIZE' => 10,
                'ROWS' => 1,
                'REGEXP' => '',
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'DEFAULT_VALUE' => '',
            ],
            'EDIT_FORM_LABEL' => [
                'en' => '',
                'ru' => 'MD5 файла импорта',
            ],
            'LIST_COLUMN_LABEL' => [
                'en' => '',
                'ru' => '',
            ],
            'LIST_FILTER_LABEL' => [
                'en' => '',
                'ru' => '',
            ],
            'ERROR_MESSAGE' => [
                'en' => '',
                'ru' => '',
            ],
            'HELP_MESSAGE' => [
                'en' => '',
                'ru' => '',
            ],
        ]);

        $helper->saveUserTypeEntity([
            'ENTITY_ID' => 'HLBLOCK_' . self::HLBLOCK_NAME,
            'FIELD_NAME' => 'UF_IMPORT_FROM',
            'USER_TYPE_ID' => 'string',
            'XML_ID' => '',
            'SORT' => '100',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'EDIT_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'SIZE' => 5,
                'ROWS' => 1,
                'REGEXP' => '',
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'DEFAULT_VALUE' => '',
            ],
            'EDIT_FORM_LABEL' => [
                'en' => '',
                'ru' => 'Из какой системы импорт',
            ],
            'LIST_COLUMN_LABEL' => [
                'en' => '',
                'ru' => '',
            ],
            'LIST_FILTER_LABEL' => [
                'en' => '',
                'ru' => '',
            ],
            'ERROR_MESSAGE' => [
                'en' => '',
                'ru' => '',
            ],
            'HELP_MESSAGE' => [
                'en' => '',
                'ru' => '',
            ],
        ]);

        $helper->saveUserTypeEntity([
            'ENTITY_ID' => 'HLBLOCK_' . self::HLBLOCK_NAME,
            'FIELD_NAME' => 'UF_CONTENT_TYPE',
            'USER_TYPE_ID' => 'string',
            'XML_ID' => '',
            'SORT' => '100',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'Y',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'EDIT_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'SIZE' => 5,
                'ROWS' => 1,
                'REGEXP' => '',
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'DEFAULT_VALUE' => '',
            ],
            'EDIT_FORM_LABEL' => [
                'en' => '',
                'ru' => 'Тип содержимого файла импорта',
            ],
            'LIST_COLUMN_LABEL' => [
                'en' => '',
                'ru' => '',
            ],
            'LIST_FILTER_LABEL' => [
                'en' => '',
                'ru' => '',
            ],
            'ERROR_MESSAGE' => [
                'en' => '',
                'ru' => '',
            ],
            'HELP_MESSAGE' => [
                'en' => '',
                'ru' => '',
            ],
        ]);

        $helper->saveUserTypeEntity([
            'ENTITY_ID' => 'HLBLOCK_' . self::HLBLOCK_NAME,
            'FIELD_NAME' => 'UF_UPLOAD_DATE',
            'USER_TYPE_ID' => 'datetime',
            'XML_ID' => '',
            'SORT' => '100',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'EDIT_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'DEFAULT_VALUE' => [
                    'TYPE' => 'NOW',
                    'VALUE' => '',
                ],
                'USE_SECOND' => 'Y',
            ],
            'EDIT_FORM_LABEL' => [
                'en' => '',
                'ru' => 'ДатаВремя сохранения файла импорта на стороне АИСКУ',
            ],
            'LIST_COLUMN_LABEL' => [
                'en' => '',
                'ru' => '',
            ],
            'LIST_FILTER_LABEL' => [
                'en' => '',
                'ru' => '',
            ],
            'ERROR_MESSAGE' => [
                'en' => '',
                'ru' => '',
            ],
            'HELP_MESSAGE' => [
                'en' => '',
                'ru' => '',
            ],
        ]);

        $helper->saveUserTypeEntity([
            'ENTITY_ID' => 'HLBLOCK_' . self::HLBLOCK_NAME,
            'FIELD_NAME' => 'UF_LAST_MODIFIED',
            'USER_TYPE_ID' => 'datetime',
            'XML_ID' => '',
            'SORT' => '100',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'EDIT_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'DEFAULT_VALUE' => [
                    'TYPE' => 'NOW',
                    'VALUE' => '',
                ],
                'USE_SECOND' => 'Y',
            ],
            'EDIT_FORM_LABEL' => [
                'en' => '',
                'ru' => 'ДатаВремя последнего действия',
            ],
            'LIST_COLUMN_LABEL' => [
                'en' => '',
                'ru' => '',
            ],
            'LIST_FILTER_LABEL' => [
                'en' => '',
                'ru' => '',
            ],
            'ERROR_MESSAGE' => [
                'en' => '',
                'ru' => '',
            ],
            'HELP_MESSAGE' => [
                'en' => '',
                'ru' => '',
            ],
        ]);

        $helper->saveUserTypeEntity([
            'ENTITY_ID' => 'HLBLOCK_' . self::HLBLOCK_NAME,
            'FIELD_NAME' => 'UF_IMPORT_DATE',
            'USER_TYPE_ID' => 'datetime',
            'XML_ID' => '',
            'SORT' => '100',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'EDIT_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'DEFAULT_VALUE' => [
                    'TYPE' => 'NOW',
                    'VALUE' => '',
                ],
                'USE_SECOND' => 'Y',
            ],
            'EDIT_FORM_LABEL' => [
                'en' => '',
                'ru' => 'ДатаВремя импорта данных из файла обмена в БД',
            ],
            'LIST_COLUMN_LABEL' => [
                'en' => '',
                'ru' => '',
            ],
            'LIST_FILTER_LABEL' => [
                'en' => '',
                'ru' => '',
            ],
            'ERROR_MESSAGE' => [
                'en' => '',
                'ru' => '',
            ],
            'HELP_MESSAGE' => [
                'en' => '',
                'ru' => '',
            ],
        ]);

        $helper->saveUserTypeEntity([
            'ENTITY_ID' => 'HLBLOCK_' . self::HLBLOCK_NAME,
            'FIELD_NAME' => 'UF_IMPORT_RESULT',
            'USER_TYPE_ID' => 'string',
            'XML_ID' => '',
            'SORT' => '100',
            'MULTIPLE' => 'N',
            'MANDATORY' => 'N',
            'SHOW_FILTER' => 'N',
            'SHOW_IN_LIST' => 'Y',
            'EDIT_IN_LIST' => 'Y',
            'IS_SEARCHABLE' => 'N',
            'SETTINGS' => [
                'SIZE' => 20,
                'ROWS' => 1,
                'REGEXP' => '',
                'MIN_LENGTH' => 0,
                'MAX_LENGTH' => 0,
                'DEFAULT_VALUE' => '',
            ],
            'EDIT_FORM_LABEL' => [
                'en' => '',
                'ru' => 'Результат импорта',
            ],
            'LIST_COLUMN_LABEL' => [
                'en' => '',
                'ru' => '',
            ],
            'LIST_FILTER_LABEL' => [
                'en' => '',
                'ru' => '',
            ],
            'ERROR_MESSAGE' => [
                'en' => '',
                'ru' => '',
            ],
            'HELP_MESSAGE' => [
                'en' => '',
                'ru' => '',
            ],
        ]);
    }

    public function down()
    {
        $helper = new migrationHelper();

        $helper->deleteHlblock(self::HLBLOCK_NAME);
    }

}
