<?php

use Bitrix\Main\EventManager;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\SystemException;
use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Type\DateTime;
use Bitrix\Main\FileTable;

class sib_exchange_ekseup extends CModule
{
    private $NAMESPACE = 'sib';
    private $MODULE = 'exchange.ekseup';
    private $MODULE_PATH;

    private const MODULE_FILES_DIR = '/upload/exchange.ekseup';
    private const EXTERNAL_FILES_DIR = 'external_files';
    private const IMPORT_LOG_FILES_DIR = 'import_log';
    private const TMP_DIR = 'tmp';

    public $MODULE_ID;
    public $MODULE_VERSION = '1.0';
    public $MODULE_VERSION_DATE = '2023-10-13 10:00:00';
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $MODULE_GROUP_RIGHTS = 'Y';
    public $PARTNER_NAME = 'Sibintek';
    public $PARTNER_URI = '';

    public function __construct()
    {
        include(__DIR__ . '/version.php');

        $this->MODULE_ID = $this->NAMESPACE . '.' . $this->MODULE;

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_NAME = 'Импорт из ЕКС и ЕУП';
        $this->MODULE_DESCRIPTION = 'Модуль импорта данных из ЕКС и ЕУП';

        $this->MODULE_PATH = $_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . $this->MODULE_ID . '/';
    }

    public function DoInstall()
    {
        global $APPLICATION;

        try {
            $this->InstallDB();
            $this->InstallEvents();
            $this->InstallFiles();
        } catch (SystemException $e) {
            $APPLICATION->ThrowException('Ошибка установки модуля: ' . $e->getMessage());
            return false;
        }

        ModuleManager::registerModule($this->MODULE_ID);

        return true;
    }

    public function DoUninstall()
    {
        global $APPLICATION;

        try {
            $this->UninstallDB();
            $this->UninstallEvents();
            $this->UninstallFiles();
        } catch (\Exception $e) {
            $APPLICATION->ThrowException('Ошибка удаления модуля: ' . $e->getMessage());
            return false;
        }

        ModuleManager::unregisterModule($this->MODULE_ID);

        return true;
    }

    public function InstallDB()
    {
        require $this->MODULE_PATH . 'install/migrationHelper.php';

        // чтение и выполнение метода up() классов с миграциями таблиц модуля
        foreach (scandir($this->MODULE_PATH . 'install/db/') as $tableClassFile) {
            if (preg_match('/\S+\.php$/ui', $tableClassFile) === 0) {
                continue;
            }

            require $this->MODULE_PATH . 'install/db/' . $tableClassFile;

            $className = str_replace('.php', '', $tableClassFile);

            if (method_exists($className, 'up')) {
                $className::up();
            }
        }

        Option::set($this->MODULE_ID, 'keep_imported_files', '30');
        Option::set($this->MODULE_ID, 'keep_not_imported_files', '10');
        Option::set($this->MODULE_ID, 'allowed_file_type', 'prod');
        Option::set($this->MODULE_ID, 'remote_addr_dev', '10.10.10.11');
        Option::set($this->MODULE_ID, 'remote_addr_tst', '10.10.10.12');
        Option::set($this->MODULE_ID, 'remote_addr_qas', '10.10.10.13');
        Option::set($this->MODULE_ID, 'remote_addr_prod', '10.10.10.14');
    }

    public function UninstallDB()
    {
        require $this->MODULE_PATH . 'install/migrationHelper.php';

        // чтение и выполнение метода down() классов с миграциями таблиц модуля
        foreach (scandir($this->MODULE_PATH . 'install/db/') as $tableClassFile) {
            if (preg_match('/\S+\.php$/ui', $tableClassFile) === 0) {
                continue;
            }

            require $this->MODULE_PATH . 'install/db/' . $tableClassFile;

            $className = str_replace('.php', '', $tableClassFile);

            if (method_exists($className, 'down')) {
                $className::down();
            }
        }

        Option::delete($this->MODULE_ID);
    }

    public function InstallEvents()
    {
        $eventManager = EventManager::getInstance();

        // Агент проверки появления новых файлов (каталог устанавливается штатной опцией import_files_dir)
        $startDateTime = new DateTime();

        $res = \CAgent::AddAgent('\\Sibintek\\Exchange\\EKSEUP\\FileHandler::checkNewFilesAgent();', $this->MODULE_ID, 'N', 3, '', 'Y', $startDateTime);

        if ($res === false) {
            throw new SystemException('Ошибка добавления агента FileHandler::checkNewFilesAgent()');
        }

        // Агент копирования сегодняшних файлов с удаленного сервера
        $startDateTime = new DateTime(date('Y-m-d') . ' 08:30:00', 'Y-m-d H:i:s');

        $res = \CAgent::AddAgent('\\Sibintek\\Exchange\\EKSEUP\\FileHandler::getRemoteFilesAgent();', $this->MODULE_ID, 'N', 24*60*60, '', 'N', $startDateTime);

        if ($res === false) {
            throw new SystemException('Ошибка добавления агента FileHandler::getRemoteFilesAgent()');
        }

        $eventManager->registerEventHandler('iblock', 'OnBeforeIBlockSectionUpdate', $this->MODULE_ID, '\Sibintek\Exchange\EKSEUP\EventHandler', 'OnBeforeIBlockSectionUpdateHandler');

        return true;
    }

    public function UnInstallEvents()
    {
        \CAgent::RemoveModuleAgents($this->MODULE_ID);

        return true;
    }

    public function InstallFiles()
    {
        // Каталог с файлами, которые вручную или агентом забираются с соответствующей шины
        $fullPath = Application::getDocumentRoot() . self::MODULE_FILES_DIR . '/' . self::EXTERNAL_FILES_DIR;

        if (!mkdir($fullPath, 0775, true) && !is_dir($fullPath)) {
            throw new SystemException(sprintf('Ошибка создания каталога для внешних файлов обмена "%s"', $fullPath));
        }
        Option::set($this->MODULE_ID, 'import_files_dir', self::MODULE_FILES_DIR . '/' . self::EXTERNAL_FILES_DIR);

        // Каталог с логами каждого импорта данных
        $fullPath = Application::getDocumentRoot() . self::MODULE_FILES_DIR . '/' . self::IMPORT_LOG_FILES_DIR;

        if (!mkdir($fullPath, 0775, true) && !is_dir($fullPath)) {
            throw new SystemException(sprintf('Ошибка создания каталога для логов импорта "%s"', $fullPath));
        }
        Option::set($this->MODULE_ID, 'log_dir', self::MODULE_FILES_DIR . '/' . self::IMPORT_LOG_FILES_DIR);

        // Каталог с временными файлами
        $fullPath = Application::getDocumentRoot() . self::MODULE_FILES_DIR . '/' . self::TMP_DIR;

        if (!mkdir($fullPath, 0775, true) && !is_dir($fullPath)) {
            throw new SystemException(sprintf('Ошибка создания каталога временных файлов "%s"', $fullPath));
        }

        CopyDirFiles(
            $this->MODULE_PATH . 'install/admin/',
            Application::getDocumentRoot() . '/bitrix/admin/',
            true,
            true
        );

        return true;
    }

    public function UnInstallFiles()
    {
        DeleteDirFilesEx(self::MODULE_FILES_DIR);

        DeleteDirFiles($this->MODULE_PATH . 'install/admin/', Application::getDocumentRoot() . '/bitrix/admin/');

        // Удаление импортированных в upload/import_ekseup XML-файлов с данными
        try {
            $files = FileTable::getList([
                'select' => ['ID'],
                'filter' => ['=MODULE_ID' => $this->MODULE_ID],
            ])->fetchAll();

            foreach ($files as $file) {
                \CFile::Delete($file['ID']);
            }
        } catch (\Exception $e) {
            // TODO Игнорировать ???
        }

        return true;
    }

}
