<?php

/**
 *
 */
const EKS_OPTIONS = [
    'SAVE_PATH' => '/eks',
    'KEEP_FILES_DAYS' => 30, // срок хранения файлов
    'DEV' => [
        'URL' => 'https://sapcid.*******.***:11111/RESTAdapter/EKSDataRequest',
        'LOGIN' => '********',
        'PASSWORD' => '********',
    ],
    'TST' => [
        'URL' => 'https://sapcit.*******.***:11112/RESTAdapter/EKSDataRequest',
        'LOGIN' => '********',
        'PASSWORD' => '********',
    ],
    'QAS' => [
        'URL' => 'https://sapciq.*******.***:11113/RESTAdapter/EKSDataRequest',
        'LOGIN' => '********',
        'PASSWORD' => '********',
    ],
    'PROD' => [
        'URL' => 'https://sapcip.*******.***:11114/RESTAdapter/EKSDataRequest',
        'LOGIN' => '********',
        'PASSWORD' => '********',
    ],
    'REQUEST' => '<?xml version="1.0" encoding="UTF-8"?>
                      <ns0:EKSDataRequest xmlns:ns0="urn:rn:AISKU:HR">
                          <UPDMOD>S</UPDMOD>
                          <PROFL>KUOG</PROFL>
                          <BUKRS>1000</BUKRS>
                          <PCKVN>94</PCKVN>
                      </ns0:EKSDataRequest>',
];

const EUP_OPTIONS = [
    'SAVE_PATH' => '/eup',
    'KEEP_FILES_DAYS' => 30, // срок хранения файлов
    'DEV' => [
        'URL' => 'https://sapcid.*******.***:22221/RESTAdapter/DataEUPRequest',
        'LOGIN' => '********',
        'PASSWORD' => '********',
    ],
    'TST' => [
        'URL' => 'https://sapcit.*******.***:22223/RESTAdapter/DataEUPRequest',
        'LOGIN' => '********',
        'PASSWORD' => '********',
    ],
    'QAS' => [
        'URL' => 'https://sapciq.*******.***:22224/RESTAdapter/DataEUPRequest',
        'LOGIN' => '********',
        'PASSWORD' => '********',
    ],
    'PROD' => [
        'URL' => 'https://sapcip.*******.***:22225/RESTAdapter/DataEUPRequest',
        'LOGIN' => '********',
        'PASSWORD' => '********',
    ],
    'REQUEST' => '<?xml version="1.0" encoding="UTF-8"?>
                      <ns0:DataEUPRequest xmlns:ns0="urn:rn:AISKU:RNP">
                      <DATE>#CURRENT_DATE#</DATE>
                      </ns0:DataEUPRequest>',
];

const LOG_OPTIONS = [
    'MAX_FILE_SIZE' => 1000, // Максимальный размер лог-файла в килобайтах
    'LOG_PATH' => '/log', // Директория с логами проекта
];

const CURL_OPTIONS = [
    ['NAME' => CURLOPT_POST, 'VALUE' => 1],
    ['NAME' => CURLOPT_RETURNTRANSFER, 'VALUE' => true],
    ['NAME' => CURLOPT_TIMEOUT, 'VALUE' => 180],
    ['NAME' => CURLOPT_CONNECTTIMEOUT, 'VALUE' => 180],
    ['NAME' => CURLOPT_FAILONERROR, 'VALUE' => true],
    ['NAME' => CURLOPT_SSL_VERIFYHOST, 'VALUE' => 0],
    ['NAME' => CURLOPT_SSL_VERIFYPEER, 'VALUE' => 0],
];