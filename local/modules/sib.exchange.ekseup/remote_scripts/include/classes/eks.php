<?php

/**
 * Класс для получения XML данных ЕКС из SAP
 *
 * Class eks
 */
class eks
{

    private $curl;
    private $curlOptions;
    private $curlResponse;
    private $xmlFile;
    private $log;

    public function __construct()
    {
        $this->log = new log();
        $this->log->setFilename('eks');
        $this->log->addMessage('Start EKS exchange ========================================================');

        $savePath = $_SERVER['DOCUMENT_ROOT'] . '/' . EKS_OPTIONS['SAVE_PATH'];

        if (!mkdir($savePath, 0775, true) && !is_dir($savePath)) {
            $this->log->addMessage('Access denied to directory ' . $savePath);
            die;
        }

        // Удаление старых файлов

        $files = scandir($savePath);
        foreach ($files as $file) {
            $ext = substr($file, -3, 3);
            if ($ext !== 'xml') {
                continue;
            }

            $type = substr($file, 0, 3);
            if ($type !== 'eks') {
                continue;
            }

            $depth = EKS_OPTIONS['KEEP_FILES_DAYS'] * 24 * 60 * 60;
            $delta = time() - filemtime($savePath . '/' . $file);
            if ($delta > $depth) {
                @unlink($savePath . '/' . $file);

                $this->log->addMessage('Deleted ' . $file);
            }
        }
    }

    public function __destruct()
    {
        $this->log->addMessage('End EKS exchange');
        $this->log->addMessage('');
    }

    /**
     * Устанавливает параметры CURL, соответствующие режиму работы - DEV, TST, QAS, PROD
     *
     * @param string $mode
     */
    public function setMode($mode = ''): void
    {
        $mode = strtoupper(trim($mode));

        $this->curlOptions = EKS_OPTIONS[$mode];

        $this->log->addMessage('Set mode = ' . $mode);
        $this->log->addMessage('CURL options: ');
        $this->log->addMessage($this->curlOptions);

        if (empty($this->curlOptions['URL'])) {
            $this->log->addMessage('Mode error, die');
            die;
        }

        $this->xmlFile = $_SERVER['DOCUMENT_ROOT'] . EKS_OPTIONS['SAVE_PATH'] . '/' . 'eks_' . strtolower(
                $mode
            ) . '_' . date('Y_m_d') . '.xml';
    }

    /**
     * Запрос данных через CURL
     */
    public function getResponse(): void
    {
        $this->curl = new curl();
        $this->curl->setAuth($this->curlOptions['LOGIN'], $this->curlOptions['PASSWORD']);
        $this->curl->setURL($this->curlOptions['URL']);
        $this->curl->setPost(EKS_OPTIONS['REQUEST']);
        $this->curl->execute();

        $this->log->addMessage('CURL Info = ' . $this->curl->getInfo());

        if ($this->curl->isSuccess() !== true) {
            $this->log->addMessage('CURL Error = ' . $this->curl->getErrorMessage());
            die;
        }

        $this->curlResponse = $this->curl->getResult();
        $this->log->addMessage('RESPONSE length = ' . strlen($this->curlResponse));

        $this->saveXML();
    }

    /**
     * Сохранение файла XML
     */
    private function saveXML(): void
    {
        $this->log->addMessage('Saving XML...');

        $size = file_put_contents($this->xmlFile, $this->curlResponse, LOCK_EX);

        $this->log->addMessage('FILE = ' . $this->xmlFile . ' --- SIZE = ' . $size . ' b');
    }

}
