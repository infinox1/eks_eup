<?php

class curl
{
    private $curl;
    private $result;
    private $info = '';
    private $errorMessage = '';
    private $isSuccess = false;

    public function __construct()
    {
        $this->curl = curl_init();

        foreach (CURL_OPTIONS as $option) {
            curl_setopt($this->curl, $option['NAME'], $option['VALUE']);
        }
    }

    public function setAuth($login = '', $password = ''): void
    {
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, ['Authorization: Basic ' . base64_encode($login . ':' . $password)]
        );
    }

    public function setURL($url = ''): void
    {
        curl_setopt($this->curl, CURLOPT_URL, $url);
    }

    public function setPost($data): void
    {
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $data);
    }

    public function execute(): void
    {
        $this->result = curl_exec($this->curl);
        $this->info = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);

        $this->isSuccess = true;

        if ((int)curl_errno($this->curl) > 0) {
            $this->isSuccess = false;
            $this->errorMessage = curl_error($this->curl);
        }
    }

    public function getInfo(): string
    {
        return $this->info;
    }

    public function getErrorMessage(): string
    {
        return $this->errorMessage;
    }

    public function isSuccess(): bool
    {
        return $this->isSuccess;
    }

    public function getResult()
    {
        return $this->result;
    }

}