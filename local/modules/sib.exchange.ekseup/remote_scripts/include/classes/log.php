<?php

class log
{

    private
        $fullPath;

    public function __construct()
    {
        $this->fullPath = $_SERVER['DOCUMENT_ROOT'] . LOG_OPTIONS['LOG_PATH'];

        if (!mkdir($concurrentDirectory = $this->fullPath, 0775, true) && !is_dir($concurrentDirectory)) {
            echo 'Log directory error' . chr(10);
            unset ($this->fullPath);
        }
    }

    /**
     * @param string $filename
     */
    public function setFilename($filename = 'noname.log'): void
    {
        if (empty($this->fullPath)) {
            exit;
        }

        $filename = preg_replace('/[^a-z0-9_\.]/ui', '', strtolower($filename));

        if (!preg_match('/(.*)\.log$/ui', $filename)) {
            $filename .= '.log';
        }

        $this->fullFilename = $this->fullPath . '/' . $filename;

        // Ротация лога, если файл существует
        if (filesize($this->fullFilename) > (LOG_OPTIONS['MAX_FILE_SIZE'] * 1024)) {
            $newName = preg_replace('/\\.[^.\\s]{3,4}$/', '', $this->fullFilename) . '_' . date('Ymd') . '.log';
            @rename($this->fullFilename, $newName);
        }

        $handle = fopen($this->fullFilename, 'a');
        if ($handle === false) {
            unset($this->fullFilename);

            echo 'Log file access problem: ' . $this->fullFilename;

            return;
        }
        fclose($handle);

        chmod($this->fullFilename, 0664);
    }

    /**
     * @param mixed $message
     */
    public function addMessage($message): void
    {
        if (empty($this->fullFilename)) {
            exit;
        }

        $time = date('Y-m-d H:i:s');

        if (is_array($message) === true) {
            $writableMessage = print_r($message, true);
        } elseif (is_object($message) === true) {
            //$tmp = json_decode(json_encode($message), true);
            $writableMessage = 'CLASS=' . get_class($message) . ' OBJECT=' . print_r($message, true);
        } elseif (is_null($message) === true) {
            $writableMessage = '<NULL>';
        } else {
            $writableMessage = (string)$message;
        }

        file_put_contents($this->fullFilename, $time . "\t" . $writableMessage . chr(10), FILE_APPEND);
    }

}