<?php

if (empty($_SERVER["DOCUMENT_ROOT"])) {
    $_SERVER["DOCUMENT_ROOT"] = dirname(__FILE__, 2);
}

if (stripos($_SERVER["DOCUMENT_ROOT"], '/') !== false && DIRECTORY_SEPARATOR !== '/') {
    $documentRoot = str_replace('/', '\\', $_SERVER["DOCUMENT_ROOT"]);
} else {
    $documentRoot = $_SERVER["DOCUMENT_ROOT"];
}

require_once __DIR__ . '/options.php';

$classesDir = $documentRoot . '/include/classes/';
foreach (scandir($classesDir) as $classFile) {
    if (preg_match('/\S+\.php$/ui', $classFile) === 0) {
        continue;
    }

    require_once $classesDir . $classFile;
}
