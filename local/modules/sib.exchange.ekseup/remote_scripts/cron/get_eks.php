<?php

require_once __DIR__ . '/../include/init.php';

// Запуск задачи из крона:
// 00 8 * * * /usr/bin/php -f /home/bitrix/www/cron/get_eks.php -- --mode=dev >> /var/log/eks.log

// Необходимо создать файл лога /var/log/eks.log с владельцем bitrix:bitrix

// Возможные варианты получения данных (указываются в ключе --mode, регистр неважен):
// DEV - разработка
// TST - тестирование
// QAS - предпрод
// PROD - прод

$options = getopt('', ['mode:']);

$eks = new eks();
$eks->setMode($options['mode']);
$eks->getResponse();
